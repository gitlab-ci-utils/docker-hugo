FROM alpine:3.20.3@sha256:beefdbd8a1da6d2915566fde36db9db0b524eb737fc57cd1367effd16dc0d06d as temp

ARG VERSION

ADD https://github.com/gohugoio/hugo/releases/download/v${VERSION}/hugo_${VERSION}_Linux-64bit.tar.gz /tmp/

WORKDIR /tmp

RUN tar -zxvf hugo*.tar.gz &&   \
    mv hugo /bin/ &&            \
    rm -rf ./* &&               \
    hugo version

FROM alpine:3.20.3@sha256:beefdbd8a1da6d2915566fde36db9db0b524eb737fc57cd1367effd16dc0d06d

ENV ENV="/etc/profile"
ENV SITE="/site"

COPY alias.sh /etc/profile.d/

COPY --from=temp /bin/hugo /bin/hugo

WORKDIR $SITE

EXPOSE 1313

LABEL org.opencontainers.image.licenses="Apache-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/docker-hugo"
LABEL org.opencontainers.image.title="docker-hugo"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/docker-hugo"

CMD ["hugo", "--cleanDestinationDir"]
