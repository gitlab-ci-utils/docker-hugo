#!/bin/sh
alias hugob="hugo --cleanDestinationDir"
alias hugos="hugo server --bind 0.0.0.0 --renderToDisk"
alias hugov="hugo version"
