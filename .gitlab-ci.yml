include:
  - project: 'gitlab-ci-utils/gitlab-ci-templates'
    ref: 35.0.0
    file:
      - '/collections/All-Projects.gitlab-ci.yml'
      - '/collections/Container-Build-Test-Deploy.gitlab-ci.yml'
      - '/templates/Container-Annotations-Overrides.gitlab-ci.yml'

variables:
  # API endpoint for the list of image tags in the docker-hugo container registry
  CURRENT_IMAGES_API: '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/649435/tags/?per_page=100'
  HUGO_VERSION: 0.135.0

# Included from Lint-Shell.gitlab-ci.yml
lint_sh:
  script:
    - shellcheck *.sh

# Override build script to pass build arg
# Included from Container-Build-Test-Deploy.gitlab-ci.yml
container_build:
  script:
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/$DOCKERFILE --build-arg VERSION=$HUGO_VERSION --destination $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG

# Smoke test to confirm hugo runs in new image
hugo_image_test:
  image: $CI_REGISTRY_IMAGE/tmp:$CI_PIPELINE_ID
  stage: container-test
  needs:
    - container_build
  variables:
    SITE_NAME: 'test-site'
  script:
    # Run hugo to create a new site to confirm successful execution
    - hugo new site $SITE_NAME
    # Confirm new site directory was created
    - cd $SITE_NAME

# Included from Container-Build-Test-Deploy.gitlab-ci.yml
.copy_image:
  needs:
    - container_scanning
    - container_dive
    - hugo_image_test
    - update_annotations

# Override to run on master branch only and deploy tag if it doesn't exist.
# Included from Container-Build-Test-Deploy.gitlab-ci.yml
deploy_tag:
  needs:
    - container_scanning
    - container_dive
    - hugo_image_test
    - hugo_image_count_check
  variables:
    IMAGE_TAG: $HUGO_VERSION
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  before_script:
    - apk add skopeo curl jq
  script:
    # Check if image already exists for the latest Hugo release
    - export IMAGE_EXISTS=$(curl -s $CURRENT_IMAGES_API | jq -r '.[].name' | grep $HUGO_VERSION || echo "")
    # If the tag image already exists, exit job without deploying
    - if [ -n "$IMAGE_EXISTS" ]; then exit 0; fi
    # Otherwise run original script
    - !reference [.copy_image, script]

# Checks registry image count to ensure max is not exceeded
hugo_image_count_check:
  image: registry.gitlab.com/gitlab-ci-utils/curl-jq:latest
  # Put in .post stage to count any new images deployed in this pipeline
  stage: .pre
  needs: []
  script:
    - |
      # Check number of current images in container registry repository.
      # If more than 100, then results in deploy_tag will be paged
      # and it may not return an accurate result for IMAGE_EXISTS.
      COUNT=$(curl -s $CURRENT_IMAGES_API | jq -r '.[].name' | wc -l)
      if [ $COUNT -ge 100 ]; then echo "More than 100 images in container registry" && exit 1; fi

prepare_release:
  needs: []

create_release:
  needs:
    - prepare_release
    - deploy_branch
